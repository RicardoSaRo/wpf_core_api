﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFfromAPI
{
    /// <summary>
    /// Interaction logic for DepartmentPage.xaml
    /// </summary>
    public partial class DepartmentPage : Window
    {
        public DepartmentPage()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Department dto = new Department();

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtDepartmentName.Text.Trim() == "")
            {
                MessageBox.Show("Please fill the text area.");
            }
            else
            {
                if (dto == null) //--> Add Department
                {
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            Department model = new Department();
                            model.DepartmentName = txtDepartmentName.Text;
                            var jsondepartment = JsonConvert.SerializeObject(model);
                            StringContent content = new StringContent(jsondepartment, Encoding.UTF8, "application/json");
                            HttpResponseMessage responseMessage = await client.PostAsync("http://localhost:49392/api/departments", content);
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                MessageBox.Show("Despartment Added Succesfully!");
                                txtDepartmentName.Clear();
                            }
                            else
                                MessageBox.Show("API Error");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Cannot Connect to API. Please verify your connection or/and API status.");
                        //throw ex;
                    }
                }
                else //--> Update Department
                {
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            dto.DepartmentName = txtDepartmentName.Text;
                            var jsondepartment = JsonConvert.SerializeObject(dto);
                            StringContent content = new StringContent(jsondepartment,Encoding.UTF8, "application/json");
                            HttpResponseMessage responseMessage = await client.PutAsync("http://localhost:49392/api/departments", content);
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                MessageBox.Show("Despartment Updated Succesfully!");
                                this.Close();
                            }
                            else
                                MessageBox.Show("Server Error");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Cannot Connect to API. Please verify your connection or/and API status.");
                    }
                }
            }
        }
                
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (dto != null)
                txtDepartmentName.Text = dto.DepartmentName;
        }
    }
}
