﻿using System;
using System.Xaml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using Newtonsoft.Json;

namespace WPFfromAPI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
            dto = null;
        }

        async void FillDataGrid()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage responseMessage = await client.GetAsync("http://localhost:49392/api/departments");
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var jsonstring = await responseMessage.Content.ReadAsStringAsync();
                        List<Department> deptList = JsonConvert.DeserializeObject<List<Department>>(jsonstring);
                        gridDepartment.ItemsSource = deptList;
                        MessageBox.Show("Despartments Loaded Succesfully!");
                    }
                    else
                        MessageBox.Show("API Error");
                }
            }
            catch (Exception)
            {
                //throw ex;
                MessageBox.Show("Cannot Connect to API. Please verify your connection or/and API status.");
            }
            
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            DepartmentPage page = new DepartmentPage();
            this.Visibility = Visibility.Hidden;
            page.ShowDialog();
            this.Visibility = Visibility.Visible;
            FillDataGrid();
        }

        Department dto = new Department();
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (dto == null)
                MessageBox.Show("Please select a department from table.");
            else
            {
                DepartmentPage page = new DepartmentPage();
                page.dto = dto;
                this.Visibility = Visibility.Hidden;
                page.ShowDialog();
                this.Visibility = Visibility.Visible;
                FillDataGrid();
            }
        }

        private void gridDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dto = (Department)gridDepartment.SelectedItem;
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dto == null)
                MessageBox.Show("Please select a department on table to delete.");
           else
            {
                if (MessageBox.Show("Are you sure you want to delete this department?","WARNING!!",MessageBoxButton.YesNo,MessageBoxImage.Warning)==MessageBoxResult.Yes)
                {
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            HttpResponseMessage responseMessage = await client.DeleteAsync("http://localhost:49392/api/departments/" + dto.DepartmentID);
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                MessageBox.Show("Despartment Deleted Succesfully!");
                                FillDataGrid();
                            }
                            else
                                MessageBox.Show("Not Found");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Cannot Connect to API. Please verify your connection or/and API status.");
                    }
                }
           }
        }
    }
}
